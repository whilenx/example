import { createSlice } from "@reduxjs/toolkit";
import { ethers } from "ethers";

const defaultProvider = () => {
  return new ethers.providers.JsonRpcProvider(
    "https://data-seed-prebsc-1-s1.binance.org:8545"
  );
};

const initialState = {
  signer: defaultProvider(),
  provider: defaultProvider(),
};

export const langSlice = createSlice({
  name: "ethState",
  initialState: initialState,
  reducers: {
    updateProvider: (state, action) => {
      state.provider = action.payload
        ? new ethers.providers.Web3Provider(action.payload)
        : defaultProvider();
    },
    //tương tự như updateProvider
    updateSigner: (state, action) => {
      //action.payload = account
      state.signer = state.provider.getSigner(action.payload);
    },
  },
});

export const { updateProvider, updateSigner } = langSlice.actions;

export default langSlice.reducer;
