import { useSelector } from "react-redux";
import { useWallet } from "use-wallet";
export const useSigner = () => {
  return useSelector((state) => state.ether.signer);
};

export const useProvider = () => {
  return useSelector((state) => state.ether.provider);
};

export const useConnect = (wallet = "injected") => {
  const { connect } = useWallet();
  return {
    onConnect: () => connect(wallet),
  };
};
