export const formatAddress = (account) => {
  return `${account.substring(0, 2)}...${account.substring(
    account.length - 4
  )}`;
};
