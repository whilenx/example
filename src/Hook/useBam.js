import { useMemo } from "react";
import { BAMtoken__factory } from "src/typechain";
import { useSigner } from "./useEth";

export const useBAMtokenERC20 = (address) => {
  const signer = useSigner();
  return useMemo(() => BAMtoken__factory.connect(address, signer), [signer]);
};
