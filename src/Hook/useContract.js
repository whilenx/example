import { BAMtoken__factory } from "src/typechain";
import React, { useEffect, useCallback, useState } from "react";
import { useWallet } from "use-wallet";
export const useApprove = (address, spender) => {
  const erc20Action = BAMtoken__factory(address);
  const [isApprove, setIsApprove] = useState(false);
  const { account } = useWallet();

  const getData = async () => {
    if (account) {
      const result = await erc20Action.approve(account, spender);
      setIsApprove(result);
    }
  };

  useEffect(() => {
    if (account) getData();
  }, [account]);

  return {
    isApprove,
    getData,
    approve: () => erc20Action.approveAll(spender),
  };
};
