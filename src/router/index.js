import { lazy } from "react";
// ** Merge Routes
const Routes = [
  {
    path: "/bam",
    component: lazy(() => import("./../page/BamPage")),
  },
];

export { Routes };
