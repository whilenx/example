import { Route, useRouteMatch } from "react-router-dom";
import BamPage from "./BamPage";
const Bam = () => {
  const match = useRouteMatch();
  return (
    <div>
      <Route exact path={`${match.path}`} component={BamPage} />
    </div>
  );
};

export default Bam;
