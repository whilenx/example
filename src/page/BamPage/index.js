import React, { useEffect, useCallback, useState } from "react";
import { useWallet } from "use-wallet";
import { ethers } from "ethers";
import { useBAMtokenERC20 } from "../../Hook/useBam";
import { contractMapping } from "../../config/index";
import { formatAddress } from "../../Hook/format";
import AppButton from "../../Component/AppButton";
import Appcard from "../../Component/AppCard";
import { Alert } from "reactstrap";
import "./style.css";
const AppBam = () => {
  const [addressMapping, setAddressMapping] = useState("");
  const [balanceOf, setBalanceOf] = useState("");
  const wallet = useWallet();
  const address = wallet.account;
  const [targetAddress, setTargetAddress] = useState("");
  const [requestPending, setRequestPending] = useState(0);
  const [listRequestPending, setListRequestPending] = useState([]);
  const [listMapTargetRequest, setListMapTargetRequest] = useState([]);
  const [countTarget, setCountTarget] = useState(0);
  const [addressTranfer, setAddressTranfer] = useState("");
  const [amountTranfer, setAmountTranfer] = useState("");
  const useBAMtoken = useBAMtokenERC20(contractMapping);

  useEffect(() => {
    async function callAddressMapping() {
      const addressMapped = await useBAMtoken.mappedAddress(address);

      setAddressMapping(addressMapped);
    }
    async function balenceOf() {
      const balenceOfOwner = await useBAMtoken.balanceOf(address);
      setBalanceOf(balenceOfOwner);
    }
    async function getRequestAddressByIndex(index) {
      const mapAddressIndex = await useBAMtoken.getPendingRequestTargetByIndex(
        address,
        index
      );
      return mapAddressIndex;
    }
    async function listPendingReqestTarget() {
      const countPending = await useBAMtoken.countPendingRequestTarget(address);
      setRequestPending(+countPending);

      const count = +countPending;
      let listResult = new Array();
      for (let i = 0; i < count; i++) {
        let result = await getRequestAddressByIndex(i.toString());

        listResult.push(result);
      }
      setListRequestPending(listResult);
    }
    async function getTargetMapAddressTargetByIndex(index) {
      const mapAddress = await useBAMtoken.getTargetMapAddressByIndex(
        address,
        index
      );
      return mapAddress;
    }
    async function listTargetMapAddress() {
      const countTargetRequest = await useBAMtoken.countTargetMapAddress(
        address
      );
      setCountTarget(+countTargetRequest);
      const count = +countTargetRequest;
      let listRequest = new Array();
      for (let i = 0; i < count; i++) {
        let result = await getTargetMapAddressTargetByIndex(i.toString());
        listRequest.push(result);
      }
      setListMapTargetRequest(listRequest);
    }

    if (address) {
      callAddressMapping();
      listPendingReqestTarget();

      balenceOf();
      listTargetMapAddress();
    }
  }, [address]);

  const handleRequestTarget = () => {
    if (targetAddress) {
      useBAMtoken.requestTarget(targetAddress);
    } else {
      alert("enter address target");
    }
  };
  const hanldeAcceptTarget = (value) => {
    if (value) {
      // useBAMtoken.approve(value, ethers.utils.parseEther("10000000000000"));
      useBAMtoken
        .acceptMapAddress(value)
        .then((res) => res)
        .catch((err) => {
          const { data } = err;
          alert(data.message);
        });
    }
  };
  const isTargetMapAddress = async (address) => {
    if (address) {
      return await useBAMtoken.isTargetMapAddress(address);
    } else {
      return false;
    }
  };
  const handleTransfer = () => {
    if (addressTranfer !== "") {
      if (amountTranfer !== "") {
        useBAMtoken
          .transferFrom(address, addressTranfer, amountTranfer)
          .then((res) => res)
          .catch((err) => console.log(err, "err"));
      } else {
        alert("enter amountTransfer  ");
      }
    } else {
      alert("enter addressTransfer  ");
    }
  };
  return (
    <div className="bam-container">
      <div>
        {/* balence: {ethers.utils.formatUnits(wallet.balance, "ether")} */}
      </div>
      <div className="bam-box">
        <div className="button-connect">
          {wallet.status === "connected" ? (
            <p className="btn" onClick={() => wallet.reset()}>
              {formatAddress(wallet.account)}{" "}
            </p>
          ) : (
            <AppButton onClick={() => wallet.connect()}>Connect</AppButton>
          )}
        </div>
        <div className="d-flex">
          <Appcard title={"Balance"}>
            <span>
              {balanceOf ? ethers.utils.formatEther(balanceOf, "ether") : ""}
            </span>
          </Appcard>
          <Appcard title={"Mapping Address"}>
            {formatAddress(addressMapping)}
          </Appcard>
        </div>
        <Appcard title={"Mapping"}>
          <input
            placeholder="target address"
            className="input"
            onChange={(e) => setTargetAddress(e.target.value)}
          />
          <div className="btn" onClick={handleRequestTarget}>
            Pending Request
          </div>
        </Appcard>
        <Appcard>
          <input
            className="input"
            placeholder="address"
            onChange={(e) => setAddressTranfer(e.target.value)}
          />
          <input
            className="input"
            placeholder="amount bam"
            onChange={(e) => setAmountTranfer(e.target.value)}
          />
          <AppButton onClick={() => handleTransfer()}>Transfer</AppButton>
        </Appcard>
        <div className="pending-request">
          {requestPending !== 0 ? (
            <div>
              <h3>Pending request</h3>
              {listRequestPending.map((value, index) => {
                return (
                  <div style={{ padding: "10px" }}>
                    <span style={{ margin: "10px" }}> {value}</span>
                    <AppButton onClick={() => hanldeAcceptTarget(value)}>
                      accept
                    </AppButton>
                  </div>
                );
              })}
            </div>
          ) : (
            ""
          )}
        </div>
        <div>
          {countTarget !== 0 ? (
            <div>
              {listMapTargetRequest.map((value, index) => {
                return (
                  <div>
                    <div>
                      <h3> Target Map Address</h3> {value}
                      <AppButton onClick={() => hanldeAcceptTarget(value)}>
                        accept
                      </AppButton>
                    </div>
                    {isTargetMapAddress(value) ? (
                      ""
                    ) : (
                      <div>
                        <h3>Target Request</h3>
                        <span style={{ fontSize: "14px", margin: "10px" }}>
                          {value}
                        </span>
                        <AppButton onClick={() => hanldeAcceptTarget(value)}>
                          accept
                        </AppButton>
                      </div>
                    )}
                  </div>
                );
              })}
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
    </div>
  );
};
export default AppBam;
