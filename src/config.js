export const RPC = {
  56: "https://bsc-dataseed1.ninicoin.io/",
  97: "https://data-seed-prebsc-1-s1.binance.org:8545",
  3: "https://ropsten.eth.aragon.network",
  4: "https://rinkeby.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161",
};

export const chainInformation = {
  56: {
    id: 1,
    chainName: "BSC Mainnet",
  },
  97: {
    id: 2,
    chainName: "BSC Testnet",
  },
};

export const chainId = 97;

export const nodes = RPC[chainId];
