import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useWallet } from "use-wallet";
import { updateProvider, updateSigner } from "./redux";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import AppBam from "./page/BamPage";
export default function App() {
  const { account, ethereum } = useWallet();
  const dispatch = useDispatch();
  useEffect(() => {
    if (ethereum) {
      dispatch(updateProvider(ethereum));
    }
    if (account) {
      dispatch(updateSigner(account));
    }
  }, [account, ethereum]);

  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<AppBam />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}
