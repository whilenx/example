import React from "react";
import ReactDOM from "react-dom";

import App from "./App";
import { Provider } from "react-redux";
import store from "./redux/store";
import { UseWalletProvider } from "use-wallet";
import { RPC } from "./config";
ReactDOM.render(
  <Provider store={store}>
    <UseWalletProvider
      chainId={1}
      connectors={{
        walletconnect: {
          rpcUrl: RPC,
        },
      }}
    >
      <App />
    </UseWalletProvider>
  </Provider>,
  document.getElementById("root")
);
