const AppButton = ({ onClick, type, children, disabled }) => {
  return (
    <button className="btn" onClick={onClick}>
      {children}
    </button>
  );
};
export default AppButton;
