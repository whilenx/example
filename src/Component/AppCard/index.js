const Appcard = ({ title, children }) => {
  return (
    <div className="bam-balance">
      <p style={{ fontSize: "20px" }}>{title}</p>
      {children}
    </div>
  );
};
export default Appcard;
